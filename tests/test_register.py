from context import package as p
import pytest

class NotAFile(object):
    pass
class DumbFile(p.File):
    pass

def test_register_not_a_class():
    with pytest.raises(TypeError,match='must be a class'):
        p.register_extension('.txt',0)

def test_register_not_a_file():
    with pytest.raises(TypeError,match="must extend 'File'."):
        p.register_extension('.txt',NotAFile)

def test_unavailable_extension(tmp_path):
    if '.txt' in p._EXTENSION2CLASS:
        del p._EXTENSION2CLASS['.txt']
    assert not p.supports('.txt')
    assert '.txt' not in p.supports()
    with pytest.raises(KeyError,match='Unsupported extension'):
        F = p.AutoFile(tmp_path / 'foo.txt', mode='rw')

def test_register_extension(tmp_path):
    p.register_extension('.txt',DumbFile)
    assert '.txt' in p.supports()
    assert p.supports('.txt')
    F = p.AutoFile(tmp_path / 'foo.txt', mode='rw')
    assert type(F) == DumbFile
