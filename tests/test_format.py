from context import package as p
import pytest
import collections
import json,yaml,configparser

simple_dict = {'foo': {'bar':'baz'}, 'deadbeef': {'de': 'ad', 'be': 'ef'}}
simple_ordered_dict = {'foo': {'bar':'baz'}, 'deadbeef':
        collections.OrderedDict([('de','ad'),('be','ef')]) }

def iniload(src):
    cp = configparser.ConfigParser()
    cp.read_file(src)
    cp = dict(cp)
    for k in list(cp.keys()):
        v = dict(cp[k])
        del cp[k]
        if len(v):
            cp[k] = v
    return cp

format_tests = []

for obj_in in [simple_dict,simple_ordered_dict]:
    for name in ['config.js','config.json']:
        format_tests.append((name,p.formats.js.JsonFile,
            obj_in,simple_dict,json.load))
    for name in ['config.yml','config.yaml']:
        format_tests.append((name,p.formats.yml.YamlFile,
            obj_in,simple_dict,yaml.load))
    format_tests.append(('config.ini',p.formats.ini.IniFile,
        obj_in,simple_dict,iniload))

@pytest.mark.parametrize('name,cls,obj_in,obj_out,loader',
        format_tests)
def test_object_dumping(tmp_path,
        name, cls, obj_in, obj_out, loader):
    F = p.AutoFile(tmp_path / name,mode='rw')
    assert type(F) == cls
    F.dump(obj_in)
    with F.open('r') as src:
        assert loader(src) == obj_out
    assert F.load() == obj_out
