from context import package as p
import contextlib
import pytest

@contextlib.contextmanager
def raises_if(boolval,*args,**kwargs):
    if boolval:
        with pytest.raises(*args,**kwargs):
            yield
    else:
        yield

def _test_none_file(r,w,x):
    F = p.File(mode=r+w+x)
    assert F.filename is None

@pytest.mark.parametrize('r',['','-','r'])
@pytest.mark.parametrize('w',['','-','w'])
@pytest.mark.parametrize('x',['','-','x'])
def test_none_file(r,w,x):
    with raises_if(x=='x',ValueError,match='read/write'):
        F = p.File(mode=r+w+x)
        assert F.filename is None

def test_wanted_rwx(tmp_path):
    f = tmp_path / "wanted_rwx.txt"
    with pytest.raises(ValueError,match='read/write'):
        F = p.File(f,mode='rwx')

@pytest.mark.parametrize('r',['','-','r'])
@pytest.mark.parametrize('w',['','-','w'])
def test_holed_path(tmp_path,r,w):
    f = tmp_path / "nonexistent" / "foo.txt"
    with pytest.raises(FileNotFoundError,match='No such directory'):
        F = p.File(f,mode=r+w)

@pytest.mark.parametrize('r',['','-','r'])
@pytest.mark.parametrize('w',['','-','w'])
def test_file_on_path(tmp_path,r,w):
    d = tmp_path / "file"
    d.write_text('foo')
    assert d.read_text() == 'foo'
    f = d / "foo.txt"
    with pytest.raises(TypeError,match='Not a directory'):
        F = p.File(f,mode=r+w)

@pytest.mark.parametrize('r',['','-','r'])
@pytest.mark.parametrize('w',['','-','w'])
def test_forbidden_dir(tmp_path,r,w):
    d = tmp_path / "subdir"
    d.mkdir()
    d.chmod(d.stat().st_mode & 0o7666)
    f = d / "foo.txt"
    with pytest.raises(PermissionError,match='Cannot enter directory'):
        F = p.File(str(f),mode=r+w)

@pytest.mark.parametrize('r',['','-','r'])
@pytest.mark.parametrize('w',['','-','w'])
def test_notafile(tmp_path,r,w):
    f = tmp_path / "foo.txt"
    f.mkdir()
    with pytest.raises(TypeError,match='Not a file'):
        F = p.File(f,mode=r+w)

@pytest.mark.parametrize('rw,m',[('rw',0o222),('rw',0o444),('rw',0o666),('r',0o444),('w',0o222)])
def test_norights(tmp_path,rw,m):
    f = tmp_path / "foo.txt"
    f.touch()
    f.chmod(f.stat().st_mode & (0o7777 ^ m))
    with pytest.raises(PermissionError,match=r"^No '.*' rights"):
        F = p.File(f,mode=rw)

@pytest.mark.parametrize('r',['','-','r'])
def test_readonly_dir(tmp_path,r):
    d = tmp_path / "subdir"
    d.mkdir()
    d.chmod(d.stat().st_mode & 0o7555)
    with pytest.raises(PermissionError,match='Directory not writable'):
        F = p.File(d / "foo.txt", mode=r+'w')

file_init_params = []
for subdir in map(lambda x: x << 6, range(8)):
    if subdir and not (subdir & 0o100):
        continue
    for txt in map(lambda x: x << 7, range(4)):
        for rw in ['','r','w','rw']:
            if 'r' in rw:
                if txt and not (txt & 0o400):
                    continue
            if 'w' in rw:
                if txt and not (txt & 0o200):
                    continue
                if subdir and not (subdir & 0o200):
                    continue
            file_init_params.append((subdir,txt,rw))


def test_none_file_init():
    F = p.File(mode='rw')
    with pytest.raises(AttributeError,match='No filename specified'):
        _ = F.open('rw')

@pytest.mark.parametrize('subdir,txt,rw',file_init_params)
def test_file_init(tmp_path,subdir,txt,rw):
    d = tmp_path
    later = []
    if subdir:
        d /= "subdir"
        d.mkdir()
        later.append(lambda : d.chmod(subdir))
    f = d / "foo.txt"
    CONTENT = ''
    if txt:
        f.touch()
        f.write_text('f00')
        CONTENT = 'f00'
        f.chmod(txt)
    for do in later:
        do()
    mode = ((('r' in rw) << 1) | ('w' in rw)) << 1
    F = p.File(f,mode=rw)
    assert F.access == mode
    mode = ('r' if 'r' in rw else '-') + ('w' if 'w' in rw else '-') + '-'
    assert not F.opened
    assert F.closed
    assert str(F) == "File('%s',mode='%s',closed)" % (str(f),mode)
    with raises_if('w' not in rw,PermissionError,match='Permission denied'):
        with F.open('w') as io:
            assert io.opened
            assert io.writable()
            io.write('bar')
            CONTENT = 'bar'
            assert str(io) == "File('%s',mode='%s',opened,writable)" % (str(f),mode)

    with raises_if('r' not in rw,PermissionError,match='Permission denied'):
        with F.open('r') as io:
            assert io.opened
            assert io.readable()
            assert io.read() == CONTENT
            assert str(io) == "File('%s',mode='%s',opened,readable)" % (str(f),mode)
        with F.open('r') as io:
            assert '\n'.join(iter(io)) == CONTENT
    with pytest.raises(AttributeError):
        F.load()
    with pytest.raises(AttributeError):
        F.dump()
