|pipeline| |coverage| |rtd|

.. |pipeline| image:: https://framagit.org/1ohmatr/sw/py/nzconf/badges/master/pipeline.svg

.. |coverage| image:: https://framagit.org/1ohmatr/sw/py/nzconf/badges/master/coverage.svg

.. |rtd| image:: https://readthedocs.org/projects/nzconf/badge/?version=latest

Manager for configuration files
